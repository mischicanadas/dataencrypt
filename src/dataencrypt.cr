# TODO: Write documentation for `Dataencrypt`
require "openssl/cipher"
require "kemal"
require "random/secure"
require "db"
require "mysql"

module Dotenv
  def self.load(path = ".env", set_env = true) : Hash(String, String)
    hash = process_file(path)
    if set_env
      set_env(hash)
    end
    hash
  end

  private def self.process_file(path : String)
    File.read_lines( File.expand_path path )
      .reject { |line| line =~ /^\s*(?:#.*)?$/ }
      .map    { |line|
        if line.match(/^([^#=\s]+)=(?:(?<Q>["`])((?:(?!\k<Q>|\\).|\\.)*)\k<Q>)(?:\s+(?:#.*)?)?$/)
          { $1, $3.gsub(Regex.new(%q<\\.>)) {|s| s[-1]} }
        elsif line.match(/^([^#=\s]+)='([^']+)'(?:\s+(?:#.*)?)?$/)
          { $1, $2 }
        elsif line.match(/^([^#=\s]+)=([^#\s"'](?:[^\s"']|\\.)*)(?:\s+(?:#.*)?)?$/)
          { $1, $2.gsub(Regex.new(%q<\\.>)) {|s| s[-1]} }
        else
          raise "this line in the env file #{path} was formatted incorrectly: #{line}"
        end
      }.to_h
  end

  private def self.set_env(hash : Hash(String, String))
    hash.each do |key, value|
      ENV[key] = value
    end
  end
end

Dotenv.load(path: "/var/www/domains/dataencrypt/.env")

module MySQLData
  def self.message(env)
    message = env.params.body["message"].as(String)
    email = env.params.body["email"].as(String)
    name = env.params.body["name"].as(String)

    db = DB.open "mysql://#{ENV["DATABASE_USER"]}:#{ENV["DATABASE_PASS"]}@#{ENV["DATABASE_HOST"]}/#{ENV["DATABASE_NAME"]}"
    begin

      args_message = [] of DB::Any
      args_message << message
      args_message << email
      args_message << name
      db.exec "insert into messages (Message, Email, Name) values(?,?,?)", args_message

      response = {status: "OK", message: "Message saved!"}.to_json
    rescue e
      response = {status: "ERROR", message: e.message }.to_json
    ensure
      db.close
    end
  end

  def self.encrypt(env)
    message = env.params.body["message"].as(String)
    passphrase = env.params.body["passphrase"].as(String)
    expiration = env.params.body["expiration"].as(String)
    
    db = DB.open "mysql://#{ENV["DATABASE_USER"]}:#{ENV["DATABASE_PASS"]}@#{ENV["DATABASE_HOST"]}/#{ENV["DATABASE_NAME"]}"
    begin
      encrypted = AES.encrypt(message, passphrase)
      link = Random::Secure.hex(8)

      args_links = [] of DB::Any
      args_links << link
      args_links << encrypted.hexstring
      args_links << expiration
      db.exec "insert into links (Link, MessageID, Expiration) values(?,?,?)", args_links

      response = {status: "OK",link: link, message: encrypted.hexstring}.to_json
    rescue e
      response = {status: "ERROR", message: e.message }.to_json
    ensure
      db.close
    end
  end

  def self.decrypt(env)
    messageid = env.params.body["messageid"].as(String)
    passphrase = env.params.body["passphrase"].as(String)
    
    begin
      
      encrypted_bytes = messageid.hexbytes
      decrypted = AES.decrypt(encrypted_bytes, passphrase)

      response = {status: "OK", message: decrypted}.to_json
    rescue e
      response = {status: "ERROR", message: e.message }.to_json
    end
  end

  def self.link(env)
    link = env.params.url["link"]
    messageID = ""
    expiration = ""
    
    db = DB.open "mysql://#{ENV["DATABASE_USER"]}:#{ENV["DATABASE_PASS"]}@#{ENV["DATABASE_HOST"]}/#{ENV["DATABASE_NAME"]}"
    begin
      db.query "select MessageID, date_format(Expiration,'%m/%d/%Y')as Expiration from links where Link = '#{link}' and Active = 1 and DATE(Expiration) >= DATE(NOW())" do |rs|
        rs.each do
          messageID = rs.read(String)
          expiration = rs.read(String)
        end
      end
    rescue e
      e.message
    ensure
      db.close
    end

    if messageID != ""
      render "src/views/link.ecr" , "src/layouts/base.ecr"
    else
      render "src/views/expired.ecr" , "src/layouts/base.ecr"
    end

  end

end

module AES
  def self.encrypt(data, password)
    cipher = OpenSSL::Cipher.new("aes-128-cbc")
    cipher.encrypt
    cipher.key = password
    io = IO::Memory.new
    io.write(cipher.update(data))
    io.write(cipher.final)
    io.to_slice
  end

  def self.decrypt(data, password)
    cipher = OpenSSL::Cipher.new("aes-128-cbc")
    cipher.decrypt
    cipher.key = password
    io = IO::Memory.new
    io.write(cipher.update(data))
    io.write(cipher.final)
    io.to_s
  end
end

module Dataencrypt
  VERSION = "0.1.0"

  # TODO: Put your code here

  get "/" do |env|
    render "src/views/index.ecr" , "src/layouts/base.ecr"
  end

  post "/message" do |env|
    env.response.content_type = "application/json"
    MySQLData.message(env)
  end

  get "/decrypt" do |env|
    render "src/views/decrypt.ecr" , "src/layouts/base.ecr"
  end

  get "/link/:link" do |env|
    MySQLData.link(env)
  end

  post "/encrypt" do |env|
    env.response.content_type = "application/json"
    MySQLData.encrypt(env)
  end

  post "/decrypt" do |env|
    env.response.content_type = "application/json"
    MySQLData.decrypt(env)
  end

end
Kemal.run(4000)
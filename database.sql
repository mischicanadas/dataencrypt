SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `dataencrypt` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `dataencrypt` ;

-- -----------------------------------------------------
-- Table `dataencrypt`.`links`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `dataencrypt`.`links` (
  `iLink` BIGINT NOT NULL AUTO_INCREMENT ,
  `Link` VARCHAR(450) NULL DEFAULT '' ,
  `MessageID` TEXT NULL ,
  `Expiration` TIMESTAMP NULL ,
  `Active` INT NULL DEFAULT 1 ,
  `Created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`iLink`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dataencrypt`.`messages`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `dataencrypt`.`messages` (
  `iMessage` BIGINT NOT NULL AUTO_INCREMENT ,
  `Message` TEXT NULL ,
  `Name` VARCHAR(450) NULL DEFAULT '' ,
  `Email` VARCHAR(450) NULL DEFAULT '' ,
  `Read` INT NULL DEFAULT 0 ,
  `Created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`iMessage`) )
ENGINE = InnoDB;

USE `dataencrypt` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
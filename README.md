# dataencrypt

A Crystal web app made with Kemal to encrypt / decrypt messages

## Installation
1. Open Mysql and run script database.sql it has all the database schema
2. Clone repo git clone git@bitbucket.org:mischicanadas/dataencrypt.git and run `crystal run src/dataencrypt.cr`

## Contributing

1. Fork it (<https://bitbucket.org/mischicanadas/dataencrypt/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [xtokio](https://bitbucket.org/mischicanadas) Luis Gómez - creator, maintainer
